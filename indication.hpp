namespace Indication {
  void onSetup();
  void onLoop();
  void openTheDoor();
  void closeTheDoor();
}
