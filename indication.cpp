#include "Arduino.h"
#include "indication.hpp"

namespace Indication {
  
#define RED_PIN 14
#define GREEN_PIN 15

void onSetup() {
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
}

time_t openedDoor;

void onLoop() {
  time_t currTime;
  time(&currTime);
  if (openedDoor + 10 < currTime) {
    closeTheDoor();
  }
  
}

void openTheDoor() {
  digitalWrite(RED_PIN, LOW);
  digitalWrite(GREEN_PIN, HIGH);
  time(&openedDoor);
}

void closeTheDoor() {
  digitalWrite(RED_PIN, HIGH);
  digitalWrite(GREEN_PIN, LOW);
}
  
}
